import math
import matplotlib.pyplot as plt

GAMMA = 1.4  # ratio of specifc heat for air
K = 0.75
HEADSHAPE_PARAMETER = 0.5

C1 = .7156 - .5313 * HEADSHAPE_PARAMETER + .5950 * (HEADSHAPE_PARAMETER ** 2)
C2 = .0796 + .0779 * HEADSHAPE_PARAMETER
C3 = 1.587 + .049 * HEADSHAPE_PARAMETER
C4 = .1122 + .1658 * HEADSHAPE_PARAMETER

# stagnation_pressure = ((( gamma - 1)/2) * mach**2 + 1) ** (gamma / (gamma - 1))

class Bullet:
    def __init__(self, head_length, meplat_diameter, boattail_length, cylider_length, boattail_angle):
        self.head_length = head_length
        self.meplat_diameter = meplat_diameter
        self.boattail_length = boattail_length
        self.cylider_length = cylider_length
        self.boattail_angle = boattail_angle

class CoefficientCalculator:
    def __init__(self, bullet):
        self.bullet = bullet

        self.thickness_ratio = (1 - bullet.meplat_diameter) / bullet.head_length
        self.critical_mach = (1 + 0.552 * (self.thickness_ratio)**(4/5)) ** (-1/2)

        print(self.thickness_ratio)
        print(self.critical_mach)

    def calc_head_drag_transonic(self, mach):
        return 0.368 * self.thickness_ratio ** (9/5) + (1.6 * self.thickness_ratio * (mach ** 2 - 1)) / ((GAMMA + 1) * mach ** 2)

    def calc_stagnation_pressure(self, mach):
        # there is a problem with this component, as it drasticly increases drag above certain mach
        # return ((1 + (GAMMA - 1) * mach ** 2 / 2) ** (1 / (GAMMA - 1)))

        # static pressure: https://www.grc.nasa.gov/www/k-12/airplane/normal.html
        # return ((GAMMA + 1) * mach ** 2 ) / ((GAMMA - 1) * mach ** 2 + 2)

        # total pressure: https://www.grc.nasa.gov/www/k-12/airplane/normal.html
        a = (((GAMMA + 1) * mach ** 2 ) / ((GAMMA - 1) * mach ** 2 + 2)) ** (GAMMA/(GAMMA - 1))
        b = (((GAMMA + 1)) / (2 * GAMMA * mach ** 2 - GAMMA + 1)) ** (1/(GAMMA - 1))
        return a * b


    def calc_head_drag_supersonic(self, mach):
        a = (C1 - C2 * (self.thickness_ratio ** 2))/(mach ** 2 - 1)
        b = (self.thickness_ratio * math.sqrt(mach ** 2 - 1)) ** (C3 + C4 * self.thickness_ratio)
        c = math.pi * K * self.bullet.meplat_diameter ** 2 * self.calc_stagnation_pressure(mach) / 4

        return a * b + c

    def calc_boattail_drag(self, mach):
        boattail_length = self.bullet.boattail_length
        cylider_length = self.bullet.cylider_length
        boattail_angle = self.bullet.boattail_angle

        a = math.sqrt(mach ** 2 - 1)
        _k = .85 / a

        A1 = (1 - 3*HEADSHAPE_PARAMETER / (5 * mach)) * (( 5 * self.thickness_ratio / (6 * a ) ) * (self.thickness_ratio / 2)**2 - (.7435/mach**2) * (self.thickness_ratio*mach)**(1.6))
        A = A1 * math.exp(- math.sqrt(2 / (GAMMA * mach ** 2)) * cylider_length ) + 2*math.tan(boattail_angle) / a - (((GAMMA + 1)*mach**4 - 4*(mach**2 - 1))*(math.tan(boattail_angle)**2)) / (2*(mach**2 - 1) ** 2)
        print(A)
        cbt = (4*A*math.tan(boattail_angle)/_k) * ((1 - math.exp(-_k*boattail_length)) + 2*math.tan(boattail_angle) * ((math.exp(-_k*boattail_length) * (boattail_length + 1/_k)) - 1/_k))

        return cbt

    def calc_boattail_transonic(self):
        boattail_length = self.bullet.boattail_length
        boattail_angle = self.bullet.boattail_angle

        return 4*(math.tan(boattail_angle)**2) * (1+math.tan(boattail_angle)/2) * (1 - math.exp(-2*boattail_length) + 2*math.tan(boattail_angle)*(math.exp(-2*boattail_length)*(boattail_length + 0.5) - 0.5))

def conditional_cdh(calculator, mach):
    if (mach < calculator.critical_mach):
        return 0
    elif ( mach >= calculator.critical_mach and mach < 1.1):
        return calculator.calc_head_drag_transonic(mach)
    else:
        return calculator.calc_head_drag_supersonic(mach)

def conditional_cdbt(calculator, mach):
    if (mach < calculator.critical_mach):
        return 0
    elif ( mach >= calculator.critical_mach and mach < 1.2):
        return calculator.calc_boattail_transonic()
    else:
        return calculator.calc_boattail_drag(mach)


# data from http://appliedballisticsllc.com/ballistics-educational-resources/bullet-data/
# bullet = Bullet(head_length=0.73, meplat_diameter=0.06, boattail_length=0.195, cylider_length=0.328, boattail_angle=math.radians(7.2))

# bullet = Bullet(head_length=3.0, meplat_diameter=0.0, boattail_length=1.0, cylider_length=1.7, boattail_angle=math.radians(7.2))
# bullet = Bullet(head_length=0.967, meplat_diameter=0.2, boattail_length=1.18, cylider_length=1.15, boattail_angle=math.radians(7.2))
# bullet = Bullet(head_length=3.01, meplat_diameter=0.09, boattail_length=0.58, cylider_length=2.06, boattail_angle=math.radians(7.2))

bullet = Bullet(head_length=0.65, meplat_diameter=0.07, boattail_length=0.21, cylider_length=0.26, boattail_angle=math.radians(7.2))

calculator = CoefficientCalculator(bullet)

machs = [(0.1 + mach * 0.1) for mach in range(50)]
cdh = [conditional_cdh(calculator, mach) for mach in machs]


# cdbt = [conditional_cdbt(calculator, mach) for mach in machs]

plt.plot(machs, cdh)
# plt.plot(machs, cdbt)
plt.show()

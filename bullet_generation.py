import numpy
import matplotlib.pyplot as plt
import itertools
import os
import sys

import drag
import genetic_algorithm
import real_bullets

FOLDER_PATH = 'C:/Users/Bob/Desktop/mcdrag_figures/{folder}'
IMG_PATH = 'C:/Users/Bob/Desktop/mcdrag_figures/{folder}/{name}.png'
SUBFOLDER = 'mutacja_5-15_jakosc_srednia_1-2-3mach'
# SUBFOLDER = 'test'


# mm_to_caliber = 6.7


def save_plot(filename):
    if not os.path.exists(FOLDER_PATH.format(folder=SUBFOLDER)):
        os.makedirs(FOLDER_PATH.format(folder=SUBFOLDER))

    plt.savefig(IMG_PATH.format(folder=SUBFOLDER, name=filename))


def plot_quality_over_time(_qualities):
    plt.plot([x for x in range(len(_qualities))], [-y for y in _qualities])
    plt.xlabel('Epoki')
    plt.ylabel('Jakość')
    save_plot('jakosc_w_czasie')
    plt.show()


def plot_dense_quality_over_time(qualities_array):
    for _q in qualities_array:
        plt.plot([x for x in range(len(_q))], [-y for y in _q])
    plt.xlabel('Epoki')
    plt.ylabel('Jakość')
    save_plot('gesta_jakosc_w_czasie')
    plt.show()


def plot_bullet_drag(bullet):
    machs = [(i + 1) * 0.1 for i in range(50)]
    cd0 = []
    for mach in machs:
        cd0.append(drag.calculate(mach, *bullet, 0.5)['cd0'])
    plt.plot(machs, cd0)
    plt.xlabel('Prędkość (mach)')
    plt.ylabel('Opór')
    save_plot('opor')
    plt.show()


def plot_two_bullet_drag(bullet1, bullet2):
    machs = [(i + 1) * 0.1 for i in range(50)]
    cd01, cd02 = [], []
    for mach in machs:
        cd01.append(drag.calculate(mach, *bullet1, 0.5)['cd0'])
        cd02.append(drag.calculate(mach, *bullet2, 0.5)['cd0'])

    plt.plot(machs, cd01, label="Najlepszy wynik")
    plt.plot(machs, cd02, label="Najgorszy wynik")
    plt.xlabel('Prędkość (mach)')
    plt.ylabel('Opór')
    plt.legend(loc='upper right')
    save_plot('opor_best_vs_worst')
    plt.show()


# Draw table
def plot_bullet_table(bullet):
    rowLabels = ['Bullet_diameter', 'Bullet_length', 'Head_length',
            'Boattail_length', 'Meplat_diameter', 'Band_diameter',
            'Base_diameter']
    vals = [[round(i * 6.7, 3)] for i in bullet]
    vals[0][0] = 6.7

    fig = plt.figure()
    ax = fig.add_subplot(111)

    the_table = plt.table(cellText=vals,
                          colWidths=[0.1] * 3,
                          rowLabels=rowLabels,
                          loc='center')

    the_table.auto_set_font_size(False)
    the_table.set_fontsize(16)
    the_table.scale(4, 4)

    plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    plt.tick_params(axis='y', which='both', right=False, left=False, labelleft=False)
    for pos in ['right','top','bottom','left']:
        plt.gca().spines[pos].set_visible(False)

    save_plot('tabela')
    plt.show()


# Mating pool size
num_parents_mating = 4
mutation_genes_number = 1

qualities_over_time = []

init_real_population = numpy.array([
    real_bullets.Lapua_bullet(),
    real_bullets.Hornady_vmax_bullet(),
    real_bullets.Sierra_bullet(),
    # real_bullets.Hornady_interlock_140_bullet(),
    real_bullets.Prvi_partizan(),
    real_bullets.Hornady_match()
])

# Population size
checks = [genetic_algorithm.reality_check(x) for x in init_real_population]

sol_per_pop = len(init_real_population)

"""
There might be inconsistency between the number of selected mating parents and 
number of selected individuals within the population.
In some cases, the number of mating parents are not sufficient to 
reproduce a new generation. If that occurred, the program will stop.
"""
num_possible_permutations = len(list(itertools.permutations(iterable=numpy.arange(0, num_parents_mating), r=2)))
num_required_permutations = sol_per_pop - num_possible_permutations
if (num_required_permutations > num_possible_permutations):
    print(
        "\n*Inconsistency in the selected populatiton size or number of parents.*"
        "\nImpossible to meet these criteria.\n"
    )
    sys.exit(1)

# new_population = genetic_algorithm.initial_population(n_individuals=sol_per_pop, reference_individual=target_chromosome)


takes = []
best_solutions_per_take = []

for take in range(50):
    qualities_over_time = []
    new_population = init_real_population.copy()
    best_sol = None

    print("Take " + str(take))
    for iteration in range(1000):
        # print("iteration " + str(iteration))

        # Measuring the fitness of each chromosome in the population.
        qualities = genetic_algorithm.cal_pop_fitness(new_population)
        # print('Quality : ', numpy.max(qualities), ', Iteration : ', iteration)

        qualities_over_time.append(numpy.max(qualities))

        # Selecting the best parents in the population for mating.
        parents = genetic_algorithm.select_mating_pool(new_population, qualities, num_parents_mating)

        # Generating next generation using crossover.
        new_population = genetic_algorithm.crossover(parents, n_individuals=sol_per_pop)

        new_population = genetic_algorithm.mutation(
            population=new_population,
            num_parents_mating=num_parents_mating,
            mutation_genes_number=mutation_genes_number)

        if (numpy.mod(iteration, 50) == 0):
            best_sol, q = genetic_algorithm.get_best_chromosome(qualities, new_population, save_dir=os.curdir + '//')

    # plot_quality_over_time(qualities_over_time)

    best_sol, q = genetic_algorithm.get_best_chromosome(qualities, new_population, save_dir=os.curdir + '//')
    takes.append(qualities_over_time)
    best_solutions_per_take.append({"quality": q, "bullet": best_sol})
    # print("best solution: ", best_sol)
    # print(new_population * 6.7)

average_quality_over_time = [sum(x)/len(takes) for x in zip(*takes)]

plot_quality_over_time(average_quality_over_time)
plot_dense_quality_over_time(takes)
best_solutions_per_take.sort(key=lambda x: -x["quality"])
print(best_solutions_per_take)
best_solution = best_solutions_per_take[0]
quality, bullet = best_solution["quality"], best_solution["bullet"]
plot_two_bullet_drag(bullet, best_solutions_per_take[-1]["bullet"])
plot_bullet_table(bullet)




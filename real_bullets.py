mm_to_caliber = 6.7


def Lapua_bullet():
    Bullet_diameter = 6.7
    Bullet_length = 33.1 / mm_to_caliber
    Head_length = 16.7 / mm_to_caliber
    Boattail_length = 5.5 / mm_to_caliber
    Meplat_diameter = 1.8 / mm_to_caliber
    Band_diameter = 6.7 / mm_to_caliber
    Base_diameter = 5.6 / mm_to_caliber

    return [Bullet_diameter, Bullet_length, Head_length,
            Boattail_length, Meplat_diameter, Band_diameter,
            Base_diameter]


def Sierra_bullet():
    Bullet_diameter = 6.7
    Bullet_length = 35.0 / mm_to_caliber
    Head_length = 17.5 / mm_to_caliber
    Boattail_length = 6 / mm_to_caliber
    Meplat_diameter = 1.5 / mm_to_caliber
    Band_diameter = 6.7 / mm_to_caliber
    Base_diameter = 4 / mm_to_caliber

    return [Bullet_diameter, Bullet_length, Head_length,
            Boattail_length, Meplat_diameter, Band_diameter,
            Base_diameter]


def Hornady_vmax_bullet():
    Bullet_diameter = 6.7
    Bullet_length = 25.9 / mm_to_caliber
    Head_length = 14.9 / mm_to_caliber
    Boattail_length = 3 / mm_to_caliber
    Meplat_diameter = 1 / mm_to_caliber
    Band_diameter = 6.7 / mm_to_caliber
    Base_diameter = 4.5 / mm_to_caliber

    return [Bullet_diameter, Bullet_length, Head_length,
            Boattail_length, Meplat_diameter, Band_diameter,
            Base_diameter]

def Hornady_interlock_140_bullet():
    Bullet_diameter = 6.7
    Bullet_length = 32.3 / mm_to_caliber
    Head_length = 13.5 / mm_to_caliber
    Boattail_length = 0.5 / mm_to_caliber
    Meplat_diameter = 2 / mm_to_caliber
    Band_diameter = 6.7 / mm_to_caliber
    Base_diameter = 6 / mm_to_caliber

    return [Bullet_diameter, Bullet_length, Head_length,
            Boattail_length, Meplat_diameter, Band_diameter,
            Base_diameter]

def Prvi_partizan():
    Bullet_diameter = 6.7
    Bullet_length = 30.4 / mm_to_caliber
    Head_length = 13 / mm_to_caliber
    Boattail_length = 2.8 / mm_to_caliber
    Meplat_diameter = 3 / mm_to_caliber
    Band_diameter = 6.7 / mm_to_caliber
    Base_diameter = 5.5 / mm_to_caliber

    return [Bullet_diameter, Bullet_length, Head_length,
            Boattail_length, Meplat_diameter, Band_diameter,
            Base_diameter]

def Hornady_match():
    Bullet_diameter = 6.7
    Bullet_length = 33.85 / mm_to_caliber
    Head_length = 17 / mm_to_caliber
    Boattail_length = 4 / mm_to_caliber
    Meplat_diameter = 2 / mm_to_caliber
    Band_diameter = 6.7 / mm_to_caliber
    Base_diameter = 5 / mm_to_caliber

    return [Bullet_diameter, Bullet_length, Head_length,
            Boattail_length, Meplat_diameter, Band_diameter,
            Base_diameter]

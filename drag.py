import math
import matplotlib.pyplot as plt

# Bullet_diameter = 5.7
# Bullet_length = 5.48
# Head_length = 3
# Headshape_param = 0.5
# Boattail_length = 1
# Meplat_diameter = 0
# Band_diameter = 1W
# Base_diameter = 0.754

# Bullet_diameter = 1.267
# Bullet_length = 1.267
# Head_length = 0.73
# Headshape_param = 0.9
# Boattail_length = 0.195
# Meplat_diameter = 0.06
# Band_diameter = 1.267
# Base_diameter = 0.259

# Bullet_diameter = 55.6
# Bullet_length = 3.25
# Head_length = 0.967
# Headshape_param = 0.0
# Boattail_length = 1.18
# Meplat_diameter = 0.2
# Band_diameter = 1.0
# Base_diameter = 1.63

# Bullet_diameter = 155.0
# Bullet_length = 5.65
# Head_length = 3.01
# Headshape_param = 0.5
# Boattail_length = 0.58
# Meplat_diameter = 0.09
# Band_diameter = 1.02
# Base_diameter = 0.848

# Bullet_diameter = 6.5
# Bullet_length = 1.094
# Head_length = 0.6
# Headshape_param = 0.5
# Boattail_length = 0.1
# Meplat_diameter = 0.063
# Band_diameter = 0.3
# Base_diameter = 0.233

# mm_to_caliber = 6.7
#
# # lapua mierzone przy biurku
# Bullet_diameter = 6.7
# Bullet_length = 33.1 / mm_to_caliber
# Head_length = 16.7 / mm_to_caliber
# Headshape_param = 0.5
# Boattail_length = 5.5 / mm_to_caliber
# Meplat_diameter = 1.8 / mm_to_caliber
# Band_diameter = 6.7 / mm_to_caliber
# Base_diameter = 5.6 / mm_to_caliber

# Thickness_ratio = (1 - Meplat_diameter) / Head_length

# FOR ALL LAMINAR BOUNDARY LAYER, CODE = L/L
# FOR LAMINAR NOSE, TURBULENT AFTERBODY, CODE = L/T
# FOR ALL TURBULENT BOUNDARY LAYER, CODE = T/T
Boundary_layer_code = "T/T"

def calculate(
        M,
        Bullet_diameter, Bullet_length, Head_length,
        Boattail_length, Meplat_diameter, Band_diameter,
        Base_diameter, Headshape_param
):
    # [8.62842766  4.37279395  4.76376781  5.85558014  0.1058653 - 8.93493077
    #  0.24176119]
    M2 = M ** 2
    Thickness_ratio = (1 - Meplat_diameter) / Head_length

    # HEAD
    def calc_cdh(M):
        if (M <= 1):
            P5 = (1 + .2 * M2) ** 3.5
        else:
            P5 = ((1.2 * M2) ** 3.5) * ((6 / (7 * M2 - 1)) ** 2.5)

        C15 = (M2 - 1) / (2.4 * M2)
        C16 = (1.122 * (P5 - 1) * Meplat_diameter ** 2) / M2

        if (M <= 0.91):
            C18 = 0
        elif(M >= 1.41):
            C18 = .85 * C16
        else:
            C18 = (.254 + 2.88 * C15) * C16

        # SUBSONIC / TRANSONIC
        if (M <= 1):
            X2 = (1 + .552 * (Thickness_ratio ** .8)) ** (-.5)
            if (M <= X2):
                C17 = 0
            else:
                C17 = .368 * (Thickness_ratio ** 1.8) + 1.6 * Thickness_ratio * C15
            
            Cdh = C17 + C18
            return Cdh


        #  SUPERSONIC
        if (M > 1): # supersonic
            B2 = M ** 2 - 1
            B = math.sqrt(B2)
            S4 = 1 + .368 * (Thickness_ratio ** 1.85)
            Z = B

            if (M >= S4):
                # do nothing
                nothing = 'nothing'
            else:
                Z = math.sqrt(S4 ** 2 - 1)

            R4 = 1 / (Z ** 2)

            C11 = .7156 - .5313 * Headshape_param + .595 * Headshape_param ** 2
            C12 = .0796 + .0779 * Headshape_param
            C13 = 1.587 + .049 * Headshape_param
            C14 = .1122 + .1658 * Headshape_param

            C17 = (C11 - C12 * (Thickness_ratio ** 2)) * R4 * ((Thickness_ratio * Z) ** (C13 + C14 * Thickness_ratio))
            
            Cdh = C17 + C18
            return Cdh

    # BOATTAIL
    def calc_cdbt(M):
            if (Boattail_length <= 0):
                Cdbt = 0
                return Cdbt

            else:
                T2 = (1 - Base_diameter) / (2 * Boattail_length)

                if (M <= 0.85):
                    Cdbt = 0

                elif(M <= 1):
                    T3 = 2 * T2 ** 2 + T2 ** 3
                    C15 = (M2 - 1) / (2.4 * M2)
                    E1 = math.exp((-2) * Boattail_length)
                    B4 = 1 - E1 + 2 * T2 * ((E1 * (Boattail_length + .5)) - .5)
                    Cdbt = 2 * T3 * B4 * (1 / (.564 + 1250 * C15 ** 2))

                elif(M <= 1.1):
                    T3 = 2 * T2 ** 2 + T2 ** 3
                    C15 = (M2 - 1) / (2.4 * M2)
                    E1 = math.exp((-2) * Boattail_length)
                    B4 = 1 - E1 + 2 * T2 * ((E1 * (Boattail_length + .5)) - .5)
                    Cdbt = 2 * T3 * B4 * (1.774 - 9.3 * C15)
                
                else:
                    B2 = M2 - 1
                    B = math.sqrt(B2)
                    B3 = .85 / B
                    A12 = (5 * Thickness_ratio) / (6 * B) + (.5 * Thickness_ratio) ** 2 - (.7435 / M2) * ((Thickness_ratio * M) ** 1.6)
                    A11 = (1 - ((.6 * Headshape_param) / M)) * A12
                    E2 = math.exp(((-1.1952) / M) * (Bullet_length - Head_length - Boattail_length))
                    X3 = ((2.4 * M2 ** 2 - 4 * B2) * (T2 ** 2)) / (2 * B2 ** 2)
                    A1 = A11 * E2 - X3 + ((2 * T2) / B)
                    R5 = 1 / B3
                    E3 = math.exp((-B3) * Boattail_length)
                    A2 = 1 - E3 + (2 * T2 * (E3 * (Boattail_length + R5) - R5))
                    Cdbt = 4 * A1 * T2 * A2 * R5

                return Cdbt


    # BASE
    def calc_cdb(M):
        if (M < 1):
            P2 = 1 / (1 + .1875 * M2 + .0531 * M2 ** 2)
        else:
            P2 = 1 / (1 + .2477 * M2 + .0345 * M2 ** 2)
        
        P4 = (1 + 9.000001E-02 * M2 * (1 - math.exp(Head_length - Bullet_length))) * (1 + .25 * M2 * (1 - Base_diameter))

        P1 = P2 * P4

        if(P1 >= 0):
            # do nothing
            nothing = 'nothing'
        else:
            P1 = 0

        Cdb = (1.4286 * (1 - P1) * (Base_diameter ** 2)) / M2
        return Cdb

    # ROTATING BAND
    def calc_cdrb(M):
        if (M< 0.95):
            Cdrb = (M ** 12.5) * (Band_diameter - 1)
        else:
            Cdrb = (.21 + .28 / M2) * (Band_diameter - 1)

        return Cdrb

    # SKIN FRICTION
    def calc_cdsf(M):
        ReL = 23296.3 * M * Bullet_length * Bullet_diameter
        R3 = .4343 * (math.log(ReL))
        Cfl = (1.328 / (math.sqrt(ReL))) * ((1 + .12 * M2) ** (-.12))
        Cft = (.455 / (R3 ** 2.58)) * ((1 + .21 * M2) ** (-.32))
        D5 = 1 + ((.333 + (.02 / (Head_length ** 2))) * Headshape_param)

        SWnose = 1.5708 * Head_length * D5 * (1 + (1 / (8 * Head_length ** 2)))
        SWcyl = 3.1416 * (Bullet_length - Head_length)
        SWtotal = SWnose + SWcyl
        
        if(Boundary_layer_code == "L/L"):
            C9 = 1.2732 * SWtotal * Cfl
            C10 = C9

        if(Boundary_layer_code == "L/T"):
            C9 = 1.2732 * SWtotal * Cfl
            C10 = 1.2732 * SWtotal * Cft

        if(Boundary_layer_code == "T/T"):
            C9 = 1.2732 * SWtotal * Cft
            C10 = C9

        Cdsf = (C9 * SWnose + C10 * SWcyl) / SWtotal
        return Cdsf


    cdh = calc_cdh(M)
    cdbt = calc_cdbt(M)
    cdb = calc_cdb(M)
    cdrb = calc_cdrb(M)
    cdsf = calc_cdsf(M)
    cd0 = cdh + cdbt + cdb + cdrb + cdsf

    data = {
        'cdh': cdh,
        'cdbt': cdbt,
        'cdb': cdb,
        'cdrb': cdrb,
        'cdsf': cdsf,
        'cd0': cd0
    }

    return data

#
# import real_bullets
#
#
# machs = [(i + 1) * 0.1 for i in range(50)]
# cdh = []
# cdbt = []
# cdb = []
# cdrb = []
# cdsf = []
# cd0 = []
#
# for mach in machs:
#     data = calculate(mach, *real_bullets.Hornady_interlock_140_bullet(), 0.5)
#     cdh.append(data['cdh'])
#     cdbt.append(data['cdbt'])
#     cdb.append(data['cdb'])
#     cdrb.append(data['cdrb'])
#     cdsf.append(data['cdsf'])
#     cd0.append(data['cd0'])
#
# plt.plot(machs, cd0)
# plt.show()





